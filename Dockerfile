ARG BUILDER_BASE_IMAGE=jupyter/scipy-notebook:python-3.9
FROM $BUILDER_BASE_IMAGE as builder


# Read more: https://sissopp_developers.gitlab.io/sissopp/quick_start/Installation.html

# ================================================================================
# Linux applications and libraries
# ================================================================================

USER root

RUN apt-get update \
 && apt-get install --yes --quiet --no-install-recommends \
    build-essential \
    g++ \
    gfortran \
    cmake \
    git \
    liblapack-dev \
    libblas-dev \
    zlib1g-dev \
    libboost-mpi-dev \
    libboost-serialization-dev \
    libboost-system-dev \
    libboost-filesystem-dev \
    libgtest-dev \
    coinor-clp \
    coinor-libclp-dev \
    # libnlopt-dev \
    openssh-client \
    dvipng \
 && apt-get clean \
 && rm -rf /var/lib/apt/lists/*


# ================================================================================
# Install all needed Python Packages
# ================================================================================

USER ${NB_UID}
WORKDIR /tmp/app

# Install from the requirements.txt file
COPY --chown=${NB_UID}:${NB_GID} requirements.in .
RUN pip install --no-cache-dir --requirement requirements.in  \
 && fix-permissions "${CONDA_DIR}" \
 && fix-permissions "/home/${NB_USER}"

# COPY --chown=${NB_UID}:${NB_GID} src/ src/
# COPY --chown=${NB_UID}:${NB_GID} pyproject.toml README.md ./
# RUN pip --no-cache-dir install . \
#  && fix-permissions "${CONDA_DIR}" \
#  && fix-permissions "/home/${NB_USER}"

# USER ${NB_UID}

# RUN mamba install --quiet --yes \
#     'numpy' \
#     'pandas' \
#     'scipy' \
#     'seaborn' \
#     'scikit-learn' \
#     'toml' \
#     'pytest' \
#  && mamba clean --all -f -y \
#  && fix-permissions "${CONDA_DIR}" \
#  && fix-permissions "/home/${NB_USER}"

# ================================================================================
#  SISSO++
# ================================================================================


USER root
WORKDIR /opt/sissopp

COPY 3rdparty/sissopp .

RUN mkdir build \
 && cd build \
 && cmake -C ../cmake/toolchains/gnu_param_py.cmake \
    -DEXTERNAL_BOOST=OFF \
    -DCMAKE_INSTALL_PREFIX=/opt/sissopp \
    ../ \
 && make \
 && make install \
 && fix-permissions "/opt/sissopp" \
 && fix-permissions "${CONDA_DIR}" \
 && fix-permissions "/home/${NB_USER}"

#  RUN mkdir build \
#  && cd build/ \
#  && cmake \
#     -DCMAKE_CXX_COMPILER=g++ \
#     -DCMAKE_C_COMPILER=gcc \
#     -DCMAKE_CXX_FLAGS="-O3" \
#     -DBUILD_PYTHON=ON \
#     -DBUILD_PARAMS=ON \
#     -DBUILD_EXE=OFF \
#     -DBUILD_TESTS=OFF \
#     ../ \
#  && make \
#  && make install \
#  && make test \
#  && cd ../ \
#  && rm -rf build/ \
#  && fix-permissions "/opt/sissopp"

# ================================================================================
# Setup the user
# ================================================================================

USER ${NB_UID}
WORKDIR "${HOME}"

COPY --chown=${NB_UID}:${NB_GID} notebook ./

